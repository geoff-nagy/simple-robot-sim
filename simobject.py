# simobject.py
# abstract superclass for all simulated objects

from abc import ABC, abstractmethod

# abstract SimObject class
class SimObject(ABC):

	# - - - class variables - - - #

	# - - - class methods - - - #

	# - - - public instance methods - - - #

	def __init__(self):

		# start at origin
		self.pos = [0, 0]

		# default orientation, in radians
		self.angle = 0.0

	# by default the generic object does nothing
	def update(self):
		pass

	# abstract method: render method must be overridden
	def render(self):
		raise NotImplementedError("SimObject's render() method must be overwritten, but it is not")

	# - - - private instance methods - - - #
