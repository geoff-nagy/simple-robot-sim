# simulation.py
# contains the singleton instance that runs the whole simulation at the highest level

from pysigil import Sigil
import ctypes

class Simulation(object):

		# - - - class variables - - - #

		# singleton instance
		instance = None

		# - - - class methods - - - #

		# singleton constructor
		def __new__(cls):

			# enforce singleton instance
			if Simulation.instance is None:
				Simulation.instance = object.__new__(cls)

			# return the instance
			return Simulation.instance

		# - - - public instance methods - - - #

		# configure simulated world
		def initialize(self, size, cellSize):

			# assign world size
			self.size = size							# horizontal and vertical size in pixels
			self.cellSize = cellSize					# size of planning grid in pixels
			self.center = [size[0] / 2, size[1] / 2]	# horizontal and vertical center of world

			# turn all visualizations on
			self.renderOccupancy = True
			self.renderPathPlanning = False
			self.renderWaypoints = True

			# input handling
			self.button1 = False
			self.button2 = False
			self.button3 = False

			# empty list of world objects
			self.objects = []

			# print info
			print("SIMULATION WORLD:")
			print("-- physical size : " + str(self.size[0]) + ", " + str(self.size[1]))
			print("-- cell size     : " + str(self.cellSize))

		# add an object to the simulation to be updated and rendered every time step
		def addObject(self, obj, pos):

			# add object to master list of objects
			self.objects.append(obj)

			# position the object where requested
			obj.pos = pos

		# retrieve a list of obstacles that currently exist
		def getObstacles(self):

			# empty list at first
			obstacles = []

			# iterate through all objects; if we find an Obstacle type, add it to the list
			for i in self.objects:
				if type(i).__name__ == "Obstacle":
					obstacles.append(i)

			# done; return the list
			return obstacles

		# perform a single update step
		def update(self, dt):
			self._updateObjects(dt)
			self._handleInput()

		# render the world and the objects contained within it
		def render(self):
			self._renderWorld()
			self._renderObjects()
			self._renderGUI()

		# - - - private instance methods - - - #

		# handle user input for visualization/debugging
		def _handleInput(self):

			# SIGIL
			sl = Sigil()

			# occupancy grid debugging
			if sl.slGetKey(Sigil.KEY_F1):
				if not self.button1:
					self.renderOccupancy = not self.renderOccupancy
				self.button1 = True
			else:
				self.button1 = False

			# path planning debugging
			if sl.slGetKey(Sigil.KEY_F2):
				if not self.button2:
					self.renderPathPlanning = not self.renderPathPlanning
				self.button2 = True
			else:
				self.button2 = False

			# waypoint debugging
			if sl.slGetKey(Sigil.KEY_F3):
				if not self.button3:
					self.renderWaypoints = not self.renderWaypoints
				self.button3 = True
			else:
				self.button3 = False

		# update objects in simulation by one time step
		def _updateObjects(self, dt):
			for i in self.objects:
				i.update(dt)

		# render simulation grid floor
		def _renderWorld(self):

			# grid properties
			GRID_CELL_SIZE = self.cellSize
			HALF_GRID_CELL_SIZE = GRID_CELL_SIZE / 2

			# set grid cell color
			sl = Sigil()
			sl.slSetForeColor(ctypes.c_double(0.1), ctypes.c_double(0.1), ctypes.c_double(0.1), ctypes.c_double(1.0))

			# draw grid cells
			color = 0
			for i in range(0, self.size[1], GRID_CELL_SIZE):
				color += 1		# ensure alternating rows by incrementing this every row
				for j in range(0, self.size[0], GRID_CELL_SIZE):

					# alternate grid cell colours
					color += 1
					if color % 2 == 0:
						sl.slRectangleFill(ctypes.c_double(j + HALF_GRID_CELL_SIZE), ctypes.c_double(i + HALF_GRID_CELL_SIZE), ctypes.c_double(GRID_CELL_SIZE - 1), ctypes.c_double(GRID_CELL_SIZE - 1))

		# render objects in the simulation
		def _renderObjects(self):
			for i in self.objects:
				i.render()

		# render instructions on how to use
		def _renderGUI(self):

			# instruction panel size
			PANEL_SIZE = [400, 75]

			# SIGIL instance
			sl = Sigil()

			# main panel
			sl.slSetForeColor(ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(0.7))
			sl.slRectangleFill(ctypes.c_double(10 + PANEL_SIZE[0] / 2), ctypes.c_double(10 + PANEL_SIZE[1] / 2), ctypes.c_double(PANEL_SIZE[0]), ctypes.c_double(PANEL_SIZE[1]))

			# instructions
			sl.slSetForeColor(ctypes.c_double(1.0), ctypes.c_double(1.0), ctypes.c_double(1.0), ctypes.c_double(0.7))
			sl.slText(ctypes.c_double(20), ctypes.c_double(65), b"VISUALIZATION OPTIONS:")
			sl.slText(ctypes.c_double(20), ctypes.c_double(50), b"Press F1 to toggle occupancy grid")
			sl.slText(ctypes.c_double(20), ctypes.c_double(35), b"Press F2 to toggle path-planning cell values")
			sl.slText(ctypes.c_double(20), ctypes.c_double(20), b"Press F3 to toggle waypoints")
