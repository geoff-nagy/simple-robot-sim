# main.py
# Geoff Nagy

# This is the main file of the robot-sigil-sim project. It is meant to be a very
# simple example of a robot simulator that uses a very basic form of
# path-planning. This project is really only meant to teach the basic structure
# of a robot simulator from a software-engineering/object-oriented standpoint.

# - - - imports - - - #

# for graphics
from pysigil import Sigil
import ctypes

# project-specific classes
from simulation import Simulation
from robot import Robot
from obstacle import Obstacle
from breadthfirstplanner import BreadthFirstPlanner

# other utilities
import random
import time

# - - - functions - - - #

# main function---program starts here
def main():

	# world constants
	FRAME_RATE_MS = 1.0 / 60.0		# in seconds
	WORLD_SIZE = [1280, 960]		# in pixels
	WORLD_CELL_SIZE = 40			# too high results in poor plan; too low is computationally costly
	CENTER = [WORLD_SIZE[0] / 2, WORLD_SIZE[1] / 2]

	# robot constants
	ROBOT_POS = [CENTER[0], 10]

	# obstacle constants
	NUM_OBSTACLES = 30
	MIN_OBSTACLE_SIZE = 40			# min and max range of random obstacle size, in pixels
	MAX_OBSTACLE_SIZE = 100
	MIN_OBSTACLE_Y = 200

	# print intro
	print("------------------------------------------------------------------")
	print("SIMPLE ROBOT SIMULATOR")
	print("------------------------------------------------------------------")

	# re-seed random number generator; change this value to get different results
	random.seed(0)
	# or, use this below to get a different seed every time you run:
	random.seed(time.time())

	# get singleton SIGIL instance and open graphical window
	sl = Sigil()
	sl.slWindow(WORLD_SIZE[0], WORLD_SIZE[1], b"Simple Robot Simulation", 0)

	# load a simple monotype font
	font = sl.slLoadFont(b"ttf/white_rabbit.ttf")
	sl.slSetFont(ctypes.c_int(font), ctypes.c_int(12))

	# get singleton simulation instance and start simulation
	sim = Simulation()
	sim.initialize(WORLD_SIZE, WORLD_CELL_SIZE)

	# build our planner object
	planner = BreadthFirstPlanner(WORLD_SIZE, WORLD_CELL_SIZE, sim)

	# add a new robot to the simulation
	robot = Robot(sim, planner)
	sim.addObject(robot, ROBOT_POS)

	# add some random obstacles to the simulation
	for i in range(0, NUM_OBSTACLES):

		# create obstacle of random size
		obstacle = Obstacle([random.randint(MIN_OBSTACLE_SIZE, MAX_OBSTACLE_SIZE), random.randint(MIN_OBSTACLE_SIZE, MAX_OBSTACLE_SIZE)])

		# position obstacle randomly
		sim.addObject(obstacle, [random.randint(0, WORLD_SIZE[0]), random.randint(MIN_OBSTACLE_Y, WORLD_SIZE[1])])

	# main loop; keep running until window is closed by user or the escape key is pressed
	while not sl.slShouldClose() and not sl.slGetKey(Sigil.KEY_ESCAPE):

		# grab time increment; this ensures that the simulation runs smoothly
		dt = FRAME_RATE_MS #sl.slGetDeltaTime()

		# update the simulation by a single step and buffer objects for rendering
		sim.update(dt)
		sim.render()

		# render any buffered objects
		sl.slRender()

	# loop complete; close SL window
	sl.slClose()

	# termination message
	print("SIMULATION COMPLETE")

# call our main function
main()
