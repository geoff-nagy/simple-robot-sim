# Simple Robot Sim
Geoff Nagy

A very simple example of a simulator that performs robot path planning. This project is only designed to teach the basic structure of a robot simulator from a software engineering/object orientation standpoint.

## Running

Target system is Linux. Python 3 is required. [SIGIL](http://www.libsigil.com/) is also required, although a pre-compiled executable is already included.

To run the simulation:

`python3 main.py`

and watch it go!

The robot will pick a random location, perform path planning, build waypoints, and use those waypoints to navigate to the goal. After reaching the goal (or as close as the robot can get to it), the robot will pick another random location, and so on.

## Troubleshooting

Because there are so few dependencies, you're unlikely to run into any issues. The only exception to this is SIGIL. A pre-compiled **libsigil.so** has already been included, but if you're running a significantly different system, it may not work. You should go to the [SIGIL website](http://www.libsigil.com/), clone SIGIL on your target machine, build SIGIL from source, and replace the pre-compiled **libsigil.so** with the one you compiled. Hopefully that won't be necessary, though, as it can get quite involved.

## end of readme
