# util.py
# some useful functions

import math

def normalizeRadians(rad):
	while rad > math.pi:
		rad -= math.pi * 2
	while rad < -math.pi:
		rad += math.pi * 2
	return rad
