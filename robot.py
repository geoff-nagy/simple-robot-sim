# robot.py
# represents a simple robot with a path planner and a goal position

from simobject import SimObject
from pysigil import Sigil
import ctypes
import math
import random
import util

class Robot(SimObject):

	# - - - class variables - - - #

	# - - - class methods - - - #

	# - - - public instance methods - - - #

	def __init__(self, sim, planner):

		# call superconstructor---this initializes our super class variables (such as position)
		super().__init__()

		# robot has to know about the simulation world because it needs to know where obstacles are when planning
		self.sim = sim

		# set the planner we want to use when determining how to get to a goal position
		self.planner = planner

		# no goal yet
		self.goal = [0, 0]
		self.replan = False

		# no waypoint yet
		self.waypoint = self.planner.getNextWaypoint()

		# not moving; note that more sophisticated robot dynamics will usually have something like "forward acceleration" and "angular acceleration" variables
		# instead of setting velocity directly; here, we set velocities directly because it results in a simpler example of a robot controller
		self.velocity = 0.0
		self.angularVelocity = 0.0

	def update(self, dt):
		self._replanIfNeeded()
		self._handleWaypointNavigation()
		self._move(dt)

	def render(self):
		self._renderPlan()
		self._renderRobot()

	def setGoal(self, goal):
		self.goal = goal			# assign new goal position
		self.replan = True			# indicate that the planner should re-run

	# - - - private instance methods - - - #

	def _replanIfNeeded(self):

		# setGoal() causes this to be set to True: do we need to replan our path?
		if self.replan:

			# do not plan again the next time this method is called
			self.replan = False

			# do we have a planner?
			if self.planner:

				# instruct the planner to plot a path to our goal from where we are; this will create
				# a series of waypoints for us to follow one-at-a-time
				self.planner.plan(self.pos, self.goal)

				# assign to ourselves the first waypoint the planner generated
				self.waypoint = self.planner.getNextWaypoint()

	def _handleWaypointNavigation(self):

		# distance in pixels at which we are close enough to the waypoint
		CLOSE_ENOUGH_DIST = 20
		CLOSE_ENOUGH_ANGLE = 0.1

		# movement values
		FORWARD_SPEED = 40.0
		ROTATION_SPEED = 2.0

		# don't move for now
		self.angularVelocity = 0.0
		self.velocity = 0.0

		# do we have a waypoint?
		if self.waypoint is not None:

			# get angle and distance to waypoint
			diffX = self.waypoint[0] - self.pos[0]
			diffY = self.waypoint[1] - self.pos[1]
			distOffset = math.sqrt((diffX * diffX) + (diffY * diffY))
			angleOffset = util.normalizeRadians((math.atan2(diffY, diffX)) - self.angle - (math.pi / 2))

			# are we facing the target closely enough? this is a very simple bang-bang controller
			if abs(angleOffset) < CLOSE_ENOUGH_ANGLE:

				# move forwards
				self.velocity = FORWARD_SPEED

			# we are not facing the waypoint, so we need to turn to face it
			else:

				# rotate in place in the correct direction
				if angleOffset >= 0.0:
					self.angularVelocity = ROTATION_SPEED
				else:
					self.angularVelocity = -ROTATION_SPEED

			# if we're close enough, get the next waypoint from the planner
			if distOffset < CLOSE_ENOUGH_DIST:
				self.waypoint = self.planner.getNextWaypoint()

		# no more waypoints left
		else:

			# set a new random goal anywhere in the world; this will cause our planner to plot a new set of waypoints for us
			self.setGoal([random.randint(0, self.sim.size[0]), random.randint(100, self.sim.size[1])])

	def _move(self, dt):

		# robot moves according to velocity
		self.angle = self.angle + (self.angularVelocity * dt)
		self.pos[0] += (self.velocity * dt) * math.sin(-self.angle)
		self.pos[1] += (self.velocity * dt) * math.cos(self.angle)

	# robot is rendered
	def _renderRobot(self):

		# robot rendering properties
		ROBOT_SIZE_X = 20
		ROBOT_SIZE_Y = 20
		ROBOT_HALF_SIZE_X = ROBOT_SIZE_X / 2
		ROBOT_HALF_SIZE_Y = ROBOT_SIZE_Y / 2
		WHEEL_SIZE_X = 3
		WHEEL_SIZE_Y = 6
		AXIS_LENGTH = 30

		# grab SIGIL instance
		sl = Sigil()

		# push transformation stack and transform into world space
		sl.slPush()
		sl.slTranslate(ctypes.c_double(self.pos[0]), ctypes.c_double(self.pos[1]))
		sl.slRotate(ctypes.c_double(math.degrees(self.angle)))				# self.angle is in radians, but slRotate() takes an argument in degrees

		# render main rectangle in local space
		sl.slSetForeColor(ctypes.c_double(0.1), ctypes.c_double(0.5), ctypes.c_double(0.1), ctypes.c_double(1.0))
		sl.slRectangleFill(ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(ROBOT_SIZE_X), ctypes.c_double(ROBOT_SIZE_Y))

		# render wheels in local space
		sl.slSetForeColor(ctypes.c_double(0.7), ctypes.c_double(0.7), ctypes.c_double(0.7), ctypes.c_double(1.0))
		sl.slRectangleFill(ctypes.c_double(0.0 - ROBOT_HALF_SIZE_X), ctypes.c_double(0.0 - ROBOT_HALF_SIZE_Y / 2), ctypes.c_double(WHEEL_SIZE_X), ctypes.c_double(WHEEL_SIZE_Y))
		sl.slRectangleFill(ctypes.c_double(0.0 - ROBOT_HALF_SIZE_X), ctypes.c_double(0.0 + ROBOT_HALF_SIZE_Y / 2), ctypes.c_double(WHEEL_SIZE_X), ctypes.c_double(WHEEL_SIZE_Y))
		sl.slRectangleFill(ctypes.c_double(0.0 + ROBOT_HALF_SIZE_X), ctypes.c_double(0.0 - ROBOT_HALF_SIZE_Y / 2), ctypes.c_double(WHEEL_SIZE_X), ctypes.c_double(WHEEL_SIZE_Y))
		sl.slRectangleFill(ctypes.c_double(0.0 + ROBOT_HALF_SIZE_X), ctypes.c_double(0.0 + ROBOT_HALF_SIZE_Y / 2), ctypes.c_double(WHEEL_SIZE_X), ctypes.c_double(WHEEL_SIZE_Y))

		# render front indicator triangle
		sl.slSetForeColor(ctypes.c_double(0.1), ctypes.c_double(0.5), ctypes.c_double(0.1), ctypes.c_double(1.0))
		sl.slTriangleFill(ctypes.c_double(0.0), ctypes.c_double(ROBOT_HALF_SIZE_Y + 2), ctypes.c_double(ROBOT_SIZE_X), ctypes.c_double(4))

		# render axes
		sl.slSetForeColor(ctypes.c_double(1.0), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(1.0))		# x axis
		sl.slLine(ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(AXIS_LENGTH), ctypes.c_double(0.0))
		sl.slSetForeColor(ctypes.c_double(0.0), ctypes.c_double(1.0), ctypes.c_double(0.0), ctypes.c_double(1.0))		# y axis
		sl.slLine(ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(AXIS_LENGTH))

		# pop transformation stack
		sl.slPop()

	# robot's plan, if any, is rendered
	def _renderPlan(self):

		# annotation properties
		WAYPOINT_TRIANGLE_SIZE = 16

		# retrieve SIGIL instance
		sl = Sigil()

		# if we have a planner, render any data it has to share
		if self.planner is not None:
			self.planner.render()

		# render the current waypoint, if we have one
		if self.waypoint is not None and self.sim.renderWaypoints:
			sl.slSetForeColor(ctypes.c_double(1.0), ctypes.c_double(1.0), ctypes.c_double(0.0), ctypes.c_double(1.0))
			sl.slTriangleFill(ctypes.c_double(self.waypoint[0]), ctypes.c_double(self.waypoint[1]), ctypes.c_double(WAYPOINT_TRIANGLE_SIZE), ctypes.c_double(WAYPOINT_TRIANGLE_SIZE))
