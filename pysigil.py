######################################
#PySigil
#Written by Andy P. (Icy_Viking)
#Python 3.8.4
#Sigil 0.9.0
#Icy Viking Games
#Sigil wrapper for Python
#Copyright (c) 2021
#####################################

#import ctype
import ctypes

class Sigil(object):

	# - - - class constants - - - #

	# sigil constants
	ALIGN_CENTER = 0
	ALIGN_RIGHT = 1
	ALIGN_LEFT = 2

	# sigil key constants
	KEY_ESCAPE = 256
	KEY_ENTER = 257
	KEY_TAB = 258
	KEY_BACKSPACE = 259
	KEY_INSERT = 260
	KEY_DELETE = 261
	KEY_RIGHT = 262
	KEY_LEFT = 263
	KEY_DOWN = 264
	KEY_UP = 265
	KEY_PAGE_UP = 266
	KEY_PAGE_DOWN = 267
	KEY_HOME = 268
	KEY_END = 269
	KEY_CAPS_LOCK = 280
	KEY_SCROLL_LOCK = 281
	KEY_NUM_LOCK = 282
	KEY_PRINT_SCREEN = 283
	KEY_PAUSE = 284
	KEY_F1 = 290
	KEY_F2 = 291
	KEY_F3 = 292
	KEY_F4 = 293
	KEY_F5 = 294
	KEY_F6 = 295
	KEY_F7 = 296
	KEY_F8 = 297
	KEY_F9 = 298
	KEY_F10 = 299
	KEY_F11 = 300
	KEY_F12 = 301
	KEY_F13 = 302
	KEY_F14 = 303
	KEY_F15 = 304
	KEY_F16 = 305
	KEY_F17 = 306
	KEY_F18 = 307
	KEY_F19 = 308
	KEY_F20 = 309
	KEY_F21 = 310
	KEY_F22 = 311
	KEY_F23 = 312
	KEY_F24 = 313
	KEY_F25 = 314
	KEY_KEYPAD_0 = 320
	KEY_KEYPAD_1 = 321
	KEY_KEYPAD_2 = 322
	KEY_KEYPAD_3 = 323
	KEY_KEYPAD_4 = 324
	KEY_KEYPAD_5 = 325
	KEY_KEYPAD_6 = 326
	KEY_KEYPAD_7 = 327
	KEY_KEYPAD_8 = 328
	KEY_KEYPAD_9 = 329
	KEY_KEYPAD_DECIMAL = 330
	KEY_KEYPAD_DIVIDE = 331
	KEY_KEYPAD_MULTIPLY = 332
	KEY_KEYPAD_SUBTRACT = 333
	KEY_KEYPAD_ADD = 334
	KEY_KEYPAD_ENTER = 335
	KEY_KEYPAD_EQUAL = 336
	KEY_LEFT_SHIFT = 340
	KEY_LEFT_CONTROL = 341
	KEY_LEFT_ALT = 342
	KEY_LEFT_SUPER = 343
	KEY_RIGHT_SHIFT = 344
	KEY_RIGHT_CONTROL = 345
	KEY_RIGHT_ALT = 346
	KEY_RIGHT_SUPER = 347

	#sigil mouse buttons
	MOUSE_BUTTON_1 = 0
	MOUSE_BUTTON_2 = 1
	MOUSE_BUTTON_3 = 2
	MOUSE_BUTTON_4 = 3
	MOUSE_BUTTON_5 = 4
	MOUSE_BUTTON_6 = 5
	MOUSE_BUTTON_7 = 6
	MOUSE_BUTTON_8 = 7
	MOUSE_BUTTON_LEFT = MOUSE_BUTTON_1
	MOUSE_BUTTON_RIGHT = MOUSE_BUTTON_2
	MOUSE_BUTTON_MIDDLE = MOUSE_BUTTON_3

	# singleton instance
	instance = None

	# singleton constructor
	def __new__(cls):

		# enforce singleton instance
		if Sigil.instance is None:

			# load library
			#Sigil.instance = ctypes.cdll.LoadLibrary('/usr/local/lib/libsigil.so')
			Sigil.instance = ctypes.cdll.LoadLibrary('lib/libsigil.so')

			#sigil init functions
			slWindow = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int,ctypes.c_int,ctypes.c_char_p,ctypes.c_int)
			slShowCursor = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)
			slClose = ctypes.CFUNCTYPE(ctypes.c_void_p)
			slShouldClose = ctypes.CFUNCTYPE(ctypes.c_int)

			#sigil key functions
			slGetKey = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)

			#sigil mouse functions
			slGetMouseButton = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)
			slGetMouseX = ctypes.CFUNCTYPE(ctypes.c_int)
			slGetMouseY = ctypes.CFUNCTYPE(ctypes.c_int)

			#sigil time functions
			slGetDeltaTime = ctypes.CFUNCTYPE(ctypes.c_double)
			slGetTime = ctypes.CFUNCTYPE(ctypes.c_double)

			#sigil render functions
			slRender = ctypes.CFUNCTYPE(ctypes.c_void_p)

			#sigil color functions
			slSetBackColor = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double)
			slSetForeColor = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)

			#sigil blendinf function
			slSetAdditiveBlend = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)

			#sigil transformation functions
			slPush = ctypes.CFUNCTYPE(ctypes.c_void_p)
			slPop = ctypes.CFUNCTYPE(ctypes.c_void_p)

			slTranslate = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double)
			slRotate = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double)
			slScale = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double)

			#sigil texture loading
			slLoadTexture = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_char_p)

			#sigil sound functions
			slLoadWAV = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_char_p)
			slSoundPlay = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)
			slSoundLoop = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)
			slSoundPause = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)
			slSoundStop = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)
			slSoundPauseAll = ctypes.CFUNCTYPE(ctypes.c_void_p)
			slSoundResumeAll = ctypes.CFUNCTYPE(ctypes.c_void_p)
			slSoundStopAll = ctypes.CFUNCTYPE(ctypes.c_void_p)
			slSoundPlaying = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)
			slSoundLooping = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_int)

			#sigil shape functions
			slTriangleFill = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)
			slTriangleOutline = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)

			slRectangleFill = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)
			slRectangleOutline = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)

			slCircleFill = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_int)
			slCircleOutline = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_int)

			slSemiCircleFill = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_int,ctypes.c_double)
			slSemiCircleOutline = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_int,ctypes.c_double)

			slPoint = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double)

			slLine = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)

			slSetSpriteTiling = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double)
			slSetSpriteScroll = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double)
			slSprite = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int,ctypes.c_double,ctypes.c_double,ctypes.c_double,ctypes.c_double)

			#sigil text functions
			slSetTextAlign = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)
			slGetTextWidth = ctypes.CFUNCTYPE(ctypes.c_double,ctypes.c_char_p)
			slGetTextHeight = ctypes.CFUNCTYPE(ctypes.c_double,ctypes.c_char_p)
			slLoadFont = ctypes.CFUNCTYPE(ctypes.c_int,ctypes.c_char_p)
			slSetFont = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int,ctypes.c_int)
			slSetFontSize = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_int)
			slText = ctypes.CFUNCTYPE(ctypes.c_void_p,ctypes.c_double,ctypes.c_double,ctypes.c_char_p)

		# return the instance
		return Sigil.instance
