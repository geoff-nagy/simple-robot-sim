# obstacle.py
# represents a static obstacle

from simobject import SimObject
from pysigil import Sigil
import ctypes
import math

class Obstacle(SimObject):

	# - - - class variables - - - #

	# - - - class methods - - - #

	# - - - public instance methods - - - #

	def __init__(self, size):

		# call superconstructor---this initializes our super class variables (such as position)
		super().__init__()

		# assign rectangular size
		self.size = size

	def update(self, dt):
		pass		# obstacle doesn't do anything; nothing to update

	def render(self):

		# grab SIGIL instance
		sl = Sigil()

		# push transformation stack and transform into world space
		sl.slPush()
		sl.slTranslate(ctypes.c_double(self.pos[0]), ctypes.c_double(self.pos[1]))
		sl.slRotate(ctypes.c_double(math.degrees(self.angle)))				# self.angle is in radians, but slRotate() takes an argument in degrees

		# render main rectangle in local space
		sl.slSetForeColor(ctypes.c_double(0.8), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(1.0))
		sl.slRectangleFill(ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(self.size[0]), ctypes.c_double(self.size[1]))

		# pop transformation stack
		sl.slPop()

	# - - - private instance methods - - - #
