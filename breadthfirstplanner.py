# breadthfirstplanner.py
# robot waypoint planner that uses a breadth-first pathfinding algorithm to generate waypoints to a goal location

from pysigil import Sigil
import ctypes
import numpy
import math
import random

class BreadthFirstPlanner:

	# - - - class variables - - - #

	# - - - class methods - - - #

	# - - - public instance methods - - - #

	def __init__(self, worldSize, cellSize, sim):

		# assign path planning properties
		self.worldSize = worldSize
		self.cellSize = cellSize
		self.sim = sim

		# compute size of grid in cells
		self.gridSize = [int(self.worldSize[0] / self.cellSize) + 1, int(self.worldSize[1] / self.cellSize) + 1]

		# build an empty occupancy grid; this is just a matrix of booleans representing the world broken up
		# into a grid of self.cellSize-sized pieces (i.e., our checkered world grid)
		self._rebuildOccupancyGrid([])

		# no waypoints yet
		self.goal = None
		self.waypoints = []
		self.nextWaypointIndex = 0

		# print info
		print("BREADTH-FIRST PLANNER:")
		print("-- physical size : " + str(self.worldSize[0]) + ", " + str(self.worldSize[1]))
		print("-- cell size     : " + str(self.cellSize))
		print("-- grid size     : " + str(self.gridSize[0]) + ", " + str(self.gridSize[1]))

	def plan(self, pos, goal):

		# make sure goal position is within world size
		if goal[0] >= 0 and goal[1] >= 0 and goal[0] <= self.worldSize[0] and goal[1] <= self.worldSize[1]:

			# - - - step 1: initialize occupancy grid - - - #

			# iterate through obstacles and fill in the occupancy grid
			self._rebuildOccupancyGrid(self.sim.getObstacles())
			queue = []

			# save goal position
			self.goal = goal

			# convert goal and current positions to cell positions
			posCell = [int(pos[0] / self.cellSize), int(pos[1] / self.cellSize)]
			goalCell = [int(goal[0] / self.cellSize), int(goal[1] / self.cellSize)]
			queue.append(goalCell)
			done = False
			step = 0

			# set goal cell to 0, where we want to reach, but only if the area is reachable
			if not self.occupancy[int(goalCell[0]), int(goalCell[1])]:
				self.board[int(goalCell[0]), int(goalCell[1])] = 0

			# - - - step 2: starting at the goal cell, expand outwards, counting each step we take - - - #

			# this iterates over each step we take to find the goal
			while not done and len(queue) > 0:

				# advance current step number
				step += 1

				# look at immediate neighbours for all cells in the queue
				cellsToProcess = len(queue)
				while not done and cellsToProcess > 0:# and len(queue) > 0:

					# grab next cell from queue
					curr = queue.pop()
					cellsToProcess -= 1

					# if this is our starting position, we're done
					if curr[0] == posCell[0] and curr[1] == posCell[1]:
						done = True

					# look north
					if self._isWalkable([curr[0] + 0, curr[1] + 1]):
						queue.insert(0, [curr[0] + 0, curr[1] + 1])
						self.board[int(curr[0] + 0)][int(curr[1] + 1)] = step

					# look east
					if self._isWalkable([curr[0] + 1, curr[1] + 0]):
						queue.insert(0, [curr[0] + 1, curr[1] + 0])
						self.board[int(curr[0] + 1)][int(curr[1] + 0)] = step

					# look south
					if self._isWalkable([curr[0] + 0, curr[1] - 1]):
						queue.insert(0, [curr[0] + 0, curr[1] - 1])
						self.board[int(curr[0] + 0)][int(curr[1] - 1)] = step

					# look west
					if self._isWalkable([curr[0] - 1, curr[1] + 0]):
						queue.insert(0, [curr[0] - 1, curr[1] + 0])
						self.board[int(curr[0] - 1)][int(curr[1] + 0)] = step

			# - - - step 3: if we reached the starting position, follow the cell numbers backwards to the goal position - - - #

			# start off with the step number at our current position's cell
			step = self.board[int(posCell[0]), int(posCell[1])] - 1
			done = False

			# keep going until we can't find any lower numbers than the current step
			while not done and step >= 0:

				# see if we can find a cell next to us that has one less step value
				nextStep = self._searchForStep(posCell, step)
				posCell = nextStep
				step -= 1

				# if we found one, add it as a waypoint; this waypoint will take us one cell closer to the goal
				if nextStep is not None:

					# add the waypoint, and remember to transform them from grid coordinates into world coordinates
					self.waypoints.append([(nextStep[0] * self.cellSize) + (self.cellSize / 2), (nextStep[1] * self.cellSize) + (self.cellSize / 2)])

				else:

					# no more steps were found; we can't generate any more waypoints
					done = True

		else:
			print("BreadthFirstPlanner.plan() cannot plot a path to " + str(goalX) + ", " + str(goalY) + " because it is out of bounds")

	def getNextWaypoint(self):

		# no waypoint by default
		result = None

		# do we have any waypoints left?
		if self.nextWaypointIndex < len(self.waypoints):
			result = self.waypoints[self.nextWaypointIndex]		# prepare to return latest waypoint
			self.nextWaypointIndex += 1							# prepare next waypoint index

		return result			# returns a waypoint if one exists, or None if there are no more left

	def render(self):
		self._renderOccupancyGrid()
		self._renderWaypoints()

	# - - - private instance methods - - - #

	# a walkable cell is one that is within the occupancy grid, not previously seen (i.e., set to -1), and also not marked as occupied (say, by an obstacle)
	def _isWalkable(self, pos):
		return pos[0] >= 0 and pos[0] < self.gridSize[0] and pos[1] >= 0 and pos[1] < self.gridSize[1] and self.board[int(pos[0])][int(pos[1])] == -1 and not self.occupancy[int(pos[0])][int(pos[1])]

	# given a particular cell and a target step count, search north-east-south-west neighbouring cells for that step value
	def _searchForStep(self, cell, step):

		# look north
		if self.board[int(cell[0] + 0), int(cell[1] + 1)] == step:
			return [cell[0] + 0, cell[1] + 1]

		# look east
		if self.board[int(cell[0] + 1), int(cell[1] + 0)] == step:
			return [cell[0] + 1, cell[1] + 0]

		# look south
		if self.board[int(cell[0] + 0), int(cell[1] - 1)] == step:
			return [cell[0] + 0, cell[1] - 1]

		# look west
		if self.board[int(cell[0] - 1), int(cell[1] + 0)] == step:
			return [cell[0] - 1, cell[1] + 0]

		return None

	def _rebuildOccupancyGrid(self, obstacles):

		# re-allocate occupancy grid and set it to all False
		self.occupancy = numpy.full((self.gridSize[0], self.gridSize[1]), False)

		# build a numbered grid for our breath-first path-planning algorithm; initialize all cells to -1, which means "not processed yet"
		self.board = numpy.full((self.gridSize[0], self.gridSize[1]), -1)

		# iterate through occupancy grid
		for y in range(0, self.gridSize[1]):
			for x in range(0, self.gridSize[0]):

				# iterate through all obstacles and set occupancy grid squares to True wherever they overlap an obstacle
				for i in obstacles:

					# compute obstacle bounds in grid cell space
					halfSizeX = i.size[0] / 2
					halfSizeY = i.size[1] / 2
					minX = int((i.pos[0] - halfSizeX) / self.cellSize)
					maxX = int((i.pos[0] + halfSizeX) / self.cellSize)
					minY = int((i.pos[1] - halfSizeY) / self.cellSize)
					maxY = int((i.pos[1] + halfSizeY) / self.cellSize)

					# if this cell falls within those bounds, set this grid cell to True in the occupancy grid, meaning that it is occupied
					if x >= minX and x <= maxX and y >= minY and y <= maxY:
						self.occupancy[x][y] = True

	def _renderOccupancyGrid(self):

		# some useful constants
		HALF_GRID_CELL_SIZE = self.cellSize / 2

		# set color to red; we only display portions of the grid that are non-zero
		sl = Sigil()

		# render squares where occupancy grid values are non-zero
		for y in range(0, self.gridSize[1]):
			for x in range(0, self.gridSize[0]):

				# only render cell squares where we have non-False (i.e., occupied) cell values
				if self.occupancy[x][y] and self.sim.renderOccupancy:
					sl.slSetForeColor(ctypes.c_double(0.2), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(1.0))
					sl.slRectangleFill(ctypes.c_double((x * self.cellSize) + HALF_GRID_CELL_SIZE), ctypes.c_double((y * self.cellSize) + HALF_GRID_CELL_SIZE), ctypes.c_double(self.cellSize - 1), ctypes.c_double(self.cellSize - 1))

				elif self.board[x][y] >= 0 and self.sim.renderPathPlanning:

					# also render the results of the path planning step-counting algorithm
					sl.slSetForeColor(ctypes.c_double(0.4), ctypes.c_double(0.4), ctypes.c_double(0.4), ctypes.c_double(1.0))
					sl.slText(ctypes.c_double((x * self.cellSize) + HALF_GRID_CELL_SIZE), ctypes.c_double((y * self.cellSize) + HALF_GRID_CELL_SIZE), str(self.board[x][y]).encode('ascii'))

	def _renderWaypoints(self):

		# SIGIL instance
		sl = Sigil()

		# only show if user wants this
		if self.sim.renderWaypoints:

			# way points we haven't been too yet as rendered as purple dots
			sl.slSetForeColor(ctypes.c_double(0.5), ctypes.c_double(0.0), ctypes.c_double(0.5), ctypes.c_double(1.0))
			for i in range(self.nextWaypointIndex, len(self.waypoints)):
				sl.slCircleFill(ctypes.c_double(self.waypoints[i][0]), ctypes.c_double(self.waypoints[i][1]), ctypes.c_double(4), ctypes.c_int(10))

		# render final goal, if we have one
		if self.goal is not None:
			sl.slSetForeColor(ctypes.c_double(1.0), ctypes.c_double(0.5), ctypes.c_double(0.0), ctypes.c_double(1.0))
			sl.slCircleFill(ctypes.c_double(self.goal[0]), ctypes.c_double(self.goal[1]), ctypes.c_double(8), ctypes.c_int(6))
